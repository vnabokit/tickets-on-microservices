import express from "express";
import { NotFoundError } from "../errors/not-found-error";

const router = express.Router();

router.get("/sd", (req, resp) => {
  throw new NotFoundError("Nothing found ehey");
});

router.post("/api/users/signout", (req, resp) => {
  resp.send("Ok signout");
});

export { router as signoutRouter };
