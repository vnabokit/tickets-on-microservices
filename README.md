## General info

This project realizes a system for tickets purchasing and is built using microservices approach. Docker and Kubernetes are applied for deployment purposes.

## Technologies

Project is created with:

- Node.js
- TypeScript
- Express.js
- Docker
- Kubernetes, Skaffold
- Google Cloud

## This project can:

- Authenticate a user (sign up, sign in)

## About microservices architecture

### Responsibilities

Eeach microservice is placed to a separate folder:

- `./auth`. Responsible for user authentication

## Setup

### Install the project locally

```
$ git clone https://gitlab.com/vnabokit/tickets-on-microservices.git
```
